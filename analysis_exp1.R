rm(list = ls(all = TRUE))
graphics.off() # clear out workspace and close opened figures
library(readr)
library(afex)
library(ggplot2)
library(lmerTest)
library(lme4)
library(dplyr)
library(patchwork)
library(sjPlot)

ggplotColours <- function(n=6, h=c(0, 360) +15){
  if ((diff(h)%%360) < 1) h[2] <- h[2] - 360/n
  hcl(h = (seq(h[1], h[2], length = n)), c = 100, l = 65)
}

theme_set(theme_bw()) # set ggplot theme to b&w
a <- read_csv("./data/dataAll_exp1.txt")
names(a) = c('suj', 'trial', 'cond', 'int', 'resp')
a$suj = as.factor(a$suj)
a$cond = as.factor(a$cond)

## exp1
a$cond = ifelse(a$cond == 1, 'C', ifelse(a$cond == 2, 'V', 'VT'))
a = a[!is.element(a$suj, c(4, 7, 14, 15)),]

## test gender effects
demo =  read_delim("./data/demo_exp1.csv",
                   ";",
                   escape_double = FALSE,
                   trim_ws = TRUE)

demo$suj = as.factor(demo$suj)
a = inner_join(a, demo, by = 'suj')

isnorm = a %>% group_by(suj, sex) %>% summarise(resp = mean(resp))
shapiro.test(isnorm$resp)
a %>% group_by(suj, sex) %>% summarise(resp = mean(resp)) %>% t.test(resp ~ sex, data = .)

m4 = glmer(
  resp ~ int * cond + (int + cond | suj),
  data = a,
  family =  binomial(link = 'probit'),
  nAGQ = 0,
)
Anova(m4, type = 3)
summary(m4)

# average fit
theme_set(theme_bw(base_size = 16))

ptmp = plot_model(
  m4,
  type = "pred",
  terms = c('int [all]', 'cond'),
  title = '',
  colors = ggplotColours(3)
)

glmplot =  ptmp + labs(x = '', y = 'Response') + theme(axis.text.y = element_text(angle = 90, hjust = .5)) +
  scale_y_continuous(labels = c('same', 'different'),
                     breaks = c(0, 1)) + xlim(0, 0.6) + scale_x_continuous(breaks = NULL) +
  scale_color_discrete(labels = c('Control tactile', 'Visual only', 'Experimental tactile')) +
  theme_minimal() + theme(legend.position = c(0.9, 0.2),
                          legend.title = element_blank())

ptop = ggplot(a[a$resp == 1,], aes(
  x = int,
  y = ..count..,
  color = cond,
  fill = cond
)) + xlim(0, 0.6) +
  geom_histogram(position = 'dodge',
                 alpha = .1,
                 bins = 50) + labs(x = "", y = "") + scale_x_continuous(breaks = NULL)  +
  scale_y_continuous(breaks = NULL, labels = c(0, 1, 2)) + theme_minimal() +
  theme(legend.position = 'none')

pbottom = ggplot(a[a$resp == 0,], aes(
  x = int,
  y = ..count..,
  color = cond,
  fill = cond
)) + xlim(0, 0.6) +
  geom_histogram(position = 'dodge',
                 alpha = .1,
                 bins = 50) + labs(y = "", x = "Speed difference (cycle/s)")  +
  scale_y_reverse(breaks = NULL, labels = c(0, 1, 2)) + theme_minimal() + theme(legend.position =
                                                                                  'none') #+ scale_y_continuous()

final_p1 = ptop + glmplot + pbottom + plot_layout(height = c(0.3, 2, 0.3), ncol = 1)

# ggsave(filename='./final_exp1_new.pdf',final_p1,device='pdf',width=8,height=5.5)